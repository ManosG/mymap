package map.mymap;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;



public class Globals {
    /*
     * Places
     */
    private static ArrayList<MarkerOptions> places = new ArrayList<MarkerOptions>();
    public static final String PLACES_URL = "http://83.212.97.209:81/test/getdata2.php";
    public static final String ANSWERS_URL = "http://83.212.97.209:81/test/savedata2.php";

    public static MarkerOptions getMarker(Place pl){
        MarkerOptions options = new MarkerOptions();
        String point = pl.getSt_astext();
        //"POINT(51.524301 -0.134536)"
        System.out.println("point"+point);
        String pArray[]=point.split(",");
        String lat = pArray[0].replace("(","");
        String lon = pArray[1].replace(")","");
        LatLng loc = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
        options.position(loc);
        String snippet = "";
        snippet = snippet + "Question!";
        options.snippet(snippet);
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

        return options;
    }
    /*
     * Getters - Setters
     */
    public static synchronized ArrayList<MarkerOptions> getPlaces() {
        return places;
    }
    public static synchronized void setPlaces(List<Place> unformatted) {
        getPlaces().clear();

        for(Place pl : unformatted){
            try{
                getPlaces().add(getMarker(pl));
            }catch(Exception e){e.printStackTrace();}
        }
    }

}
