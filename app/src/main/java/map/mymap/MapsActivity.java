package map.mymap;

import android.app.Activity;
import android.location.Criteria;
import android.provider.Settings.Secure;
import android.app.ProgressDialog;
import android.content.Context;
import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.location.LocationManager.NETWORK_PROVIDER;
///////////////////////////////////////////////////////////////////////
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.HashMap;
//////////////////////////////////////////////////////////////////////////

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ConfirmQuestionDialogButtonInterface {
    private static List<Place> l1;
    private static GoogleMap mMap;
    private static ConfirmQuestionDialogButtonInterface dialogInterface = null;
    private Activity context=null;
    private static Activity contextS=null;
    private static Location currentLocation = null;
    private static CameraPosition cmPositionForRotation = null;
    private static boolean firstLoc = true;
    private static LocationManager locationManager;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int LOCATION_REQUEST = 2;
    private static LocationListener locationListener = null;
    SupportMapFragment mapFragment;
    ///////////////////////////////////////////////////////////////////
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();

    // Session Manager Class
    SessionManagement session;
    /////////////////////////////////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        /////////////////////////////////////////////////////////////////////
        session = new SessionManagement(getApplicationContext());

        //TextView lblName = (TextView) findViewById(R.id.lblName);
        //TextView lblEmail = (TextView) findViewById(R.id.lblEmail);

        session.checkLogin();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // name
        String name = user.get(SessionManagement.KEY_NAME);

        // email
        String email = user.get(SessionManagement.KEY_EMAIL);

        ///////////////////////////////////////////////////////////////////
        //new GetPlacesTask().execute((Void) null);
        dialogInterface = this;
        contextS=this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        context=this;
        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {

                // Called when a new location is found by the network location provider.
                System.out.println("Mpika on Location Changed");
                setCurrentLocation(location);
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                if (firstLoc && getMap() != null) {
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
                    getMap().animateCamera(cameraUpdate);
                    firstLoc = false;
                }
                System.out.println("Getting Places");
                new GetPlacesTask().execute((Void) null);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Display UI and wait for user interaction
                locationManager.requestLocationUpdates(NETWORK_PROVIDER, 0, 0, locationListener);
            } else {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }
        } else {
            locationManager.requestLocationUpdates(NETWORK_PROVIDER, 0, 0, locationListener);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            return;
        }
        locationManager.removeUpdates(locationListener);
    }
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST) {
            System.out.println("Mpika sto equals");
            System.out.println("grantResults.length" + grantResults.length);
            if (grantResults.length == 2
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Mpika permission granted");
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(NETWORK_PROVIDER, 0, 0, locationListener);
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Criteria criteria;
                String bestProvider;
                criteria = new Criteria();
                bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true)).toString();
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                if (firstLoc && getMap() != null) {
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
                    getMap().animateCamera(cameraUpdate);
                    firstLoc = false;
                }
                System.out.println("Getting Places");
                new GetPlacesTask().execute((Void) null);
            } else {
                // Permission was denied or request was cancelled
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            //setCurrentLocation(location);
            setCurrentLocation(mMap.getMyLocation());

        } else {
            // Show rationale and request permission.
        }
        // Perform any camera updates here
        getMap().setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition arg0) {
                setCmPositionForRotation(arg0);
            }
        });

        restorePolys();
        restoreCamera();
        getMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                System.out.println("Marker Clicked");
                double pointLat = marker.getPosition().latitude;
                double pointLon = marker.getPosition().longitude;
                String pointXY = "("+pointLat+","+pointLon+")";
                String message="";
                for(Place p : l1)
                {
                    if(p.getSt_astext().equals(pointXY))
                    {
                        System.out.println("vrika to simeio");
                        message=p.getQuestion()+";"+p.getAnswer1()+";"+p.getAnswer2()+";"+p.getAnswer3()+";"+p.getAnswer4()+";"+p.getId()+";"+p.getCorrect();
                    }
                }
                String confirmButton = getString(R.string.valid_dialog_confirm_button);
                String cancelButton = getString(R.string.invalid_dialog_confirm_button);
                QuestionDialogCreator dialog = new QuestionDialogCreator(context,
                        dialogInterface, message, confirmButton, cancelButton);
                dialog.showDialog();
                // Return false to indicate that we have not consumed the event and that we wish
                // for the default behavior to occur (which is for the camera to move such that the
                // marker is centered and for the marker's info window to open, if it has one).
                return false;
            }
        });
    }

    public static void restorePolys(){
        if(getMap() == null)
            return;
        //Add places
        try{
            for(MarkerOptions mo : Globals.getPlaces())
                getMap().addMarker(mo);
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }
    public static void restoreCamera() {
        //restore camera
        try {
            if (getCmPositionForRotation() != null) {
                CameraUpdate cmUpdate = CameraUpdateFactory.newCameraPosition(getCmPositionForRotation());
                getMap().animateCamera(cmUpdate, 1, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static Location getCurrentLocation() {
        return currentLocation;
    }

    public static void setCurrentLocation(Location currentLocation) {
        MapsActivity.currentLocation = currentLocation;
    }
    public static GoogleMap getMap() {
        return mMap;
    }
    public static CameraPosition getCmPositionForRotation() {
        return cmPositionForRotation;
    }

    public static void setCmPositionForRotation(CameraPosition cmPositionForRotation) {
        MapsActivity.cmPositionForRotation = cmPositionForRotation;
    }
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//                System.out.println("Marker Clicked");
//                String confirmButton = getString(R.string.valid_dialog_confirm_button);
//                String cancelButton = getString(R.string.invalid_dialog_confirm_button);
//                QuestionDialogCreator dialog = new QuestionDialogCreator(context,
//                        dialogInterface, "", confirmButton, cancelButton);
//                dialog.showDialog();
//                // Return false to indicate that we have not consumed the event and that we wish
//                // for the default behavior to occur (which is for the camera to move such that the
//                // marker is centered and for the marker's info window to open, if it has one).
//                return false;
//            }
//        });
//    }

    @Override
    public void positiveButton(String answer, String id) {
        //store answer to database
        String correct = "";
        for(Place p : l1)
        {
            if(p.getId().equals(id))
            {
                System.out.println(p.getCorrect());
                if(p.getCorrect().equals(answer))
                {
                    //the user gave the correct answer
                    correct="Yes";
                    Toast.makeText(context.getApplicationContext(), "Your answer is correct", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //the user gave the wrong answer
                    correct="No";
                    String c = "Your answer is wrong. The correct answer is: "+p.getCorrect();
                    Toast.makeText(context.getApplicationContext(), c, Toast.LENGTH_LONG).show();
                }
            }
        }
        new SaveAnswersTask(answer, id, correct).execute();
    }

    @Override
    public void negativeButton() {

    }
    /*
	 * *Async Task to update profile
	 * *
	 */
    public class SaveAnswersTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog m_dialog = null;
        boolean connectivityError = false;
        String a, i, c;
        public SaveAnswersTask(String answer, String id, String correct) {
            this.a = answer;
            this.i = id;
            this.c = correct;
        }
        @Override
        protected void onPreExecute() {
            try{
                context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

                m_dialog = new ProgressDialog(context);
                m_dialog.setMessage("Saving Answers to DB! Plase Wait!");
                m_dialog.setCancelable(false);
                m_dialog.show();
            }catch(Exception e){e.printStackTrace();}
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                System.out.println("Saving answer...");
                boolean success=false;
                success = saveAnswers(a,i,c);
                return success;
            } catch (Exception e) {
                connectivityError = true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            try{
                context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                m_dialog.cancel();
            }catch(Exception e){e.printStackTrace();}
            if(connectivityError){
                Toast.makeText(context.getApplicationContext(), "Could not save your answer due to a connectivity error", Toast.LENGTH_SHORT).show();
            }
            else if(success){
                //MainActivity.setShowInfoDialog(true);
                Toast.makeText(context.getApplicationContext(), "Answer Saved Successfully", Toast.LENGTH_SHORT).show();

            }
            else{
                Toast.makeText(context.getApplicationContext(), "Could not save answer!", Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        protected void onCancelled() {}
    }

    /*
	 * Places loader
	 */
    public class GetPlacesTask extends AsyncTask<Void, Void, Boolean> {
        boolean connectivityError = false;
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                boolean success;
                System.out.println("Get places function");
                success = getPlaces(getCurrentLocation().getLatitude(),
                        getCurrentLocation().getLongitude());
                System.out.println("success"+success);
                return success;
            } catch (Exception e) {
                e.printStackTrace();
                connectivityError = true;
            }
            return false;
        }



        @Override
        protected void onPostExecute(Boolean success) {
            if(connectivityError){
                Toast.makeText(context, R.string.map_connectivity_error, Toast.LENGTH_LONG).show();
            }
            else if(success){
                Toast.makeText(context, "Places loaded.", Toast.LENGTH_SHORT).show();
                try{
                    restorePolys();
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                        return;
                    }
                    locationManager.removeUpdates(locationListener);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            else{
                Toast.makeText(context, R.string.map_other_error,
                        Toast.LENGTH_LONG).show();
            }
        }
        @Override
        protected void onCancelled() {}
    }
    public static boolean getPlaces(double latitude, double longitude) throws IOException {
        Gson gson = new Gson();
        RestResponse resp = RestConnector.makeHttpRequest(Globals.PLACES_URL, "POST", "", null);
        System.out.println(resp.getMessage());
        //Response response = gson.fromJson(resp.getMessage(), Response.class);
        Place[] p=gson.fromJson(resp.getMessage(), Place[].class);
        System.out.println("Operation Status"+p.length);
        l1 = Arrays.asList(p);
        Globals.setPlaces(l1);
        return true;
    }

    public static boolean saveAnswers(String answer, String id, String correct) throws IOException {
        Gson gson = new Gson();
        String answer2="'"+answer+"'";
        // Android Unique ID
        String androidId = Secure.getString(contextS.getContentResolver(), Settings.Secure.ANDROID_ID);
        String url = Globals.ANSWERS_URL+"?answer="+answer2+"&id="+id+"&mid="+androidId+"&correct="+correct;
        System.out.println("url"+url);
        RestResponse resp = RestConnector.makeHttpRequest(url, "POST", "", null);
        System.out.println(resp.getMessage());
        return true;
    }
}
