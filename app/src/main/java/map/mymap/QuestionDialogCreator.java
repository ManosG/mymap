package map.mymap;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;



public class QuestionDialogCreator {
    private Context context = null;
    private ConfirmQuestionDialogButtonInterface callback = null;

    private String message = null;

    /*
     * Confirmation dialog
     */
    private String positiveButtonMessage = null;
    private String negativeButtonMessage = null;

    /**
     * Make a confirmation dialog
     */
    public QuestionDialogCreator(Context context, ConfirmQuestionDialogButtonInterface callback, String message, String positiveButtonMessage,
                             String negativeButtonMessage) {
        this.context = context;
        if(context == null)
            throw new NullPointerException();
        this.callback = callback;
        this.message = message;
        this.positiveButtonMessage = positiveButtonMessage;
        this.negativeButtonMessage = negativeButtonMessage;

    }


    public void showDialog(){

        makeConfirmDialog();

    }

    private void makeConfirmDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater mInflater;
        mInflater = LayoutInflater.from(context);
        System.out.println("getMessage()"+getMessage());
        String messageAr[]=getMessage().split(";");
        alertDialogBuilder.setTitle("Question:");
        final String strid = messageAr[5];
        alertDialogBuilder.setView( mInflater.inflate(R.layout.question_dialog, null));
        //final View myFragmentView = mInflater.inflate(R.layout.datetime_dialog, container, false);
        alertDialogBuilder.setPositiveButton(getPositiveButtonMessage(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RadioGroup answer = (RadioGroup) ((AlertDialog) dialog).findViewById(R.id.radioAnswer);
                int selectedId = answer.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) ((AlertDialog) dialog).findViewById(selectedId);
                String answerStr = radioButton.getText().toString();
                dialog.dismiss();
                if(callback != null){
                    callback.positiveButton(answerStr, strid);
                }
            }
        });
        alertDialogBuilder.setNegativeButton(getNegativeButtonMessage(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if(callback != null){
                    callback.negativeButton();
                }
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        TextView Q = (TextView) alertDialog.findViewById(R.id.textView13);
        Q.setText(messageAr[0]);
        RadioButton a1 = (RadioButton) alertDialog.findViewById(R.id.radioA1);
        a1.setText(messageAr[1]);
        RadioButton a2 = (RadioButton) alertDialog.findViewById(R.id.radioA2);
        a2.setText(messageAr[2]);
        RadioButton a3 = (RadioButton) alertDialog.findViewById(R.id.radioA3);
        a3.setText(messageAr[3]);
        RadioButton a4 = (RadioButton) alertDialog.findViewById(R.id.radioA4);
        a4.setText(messageAr[4]);
    }

	/*
	 * Getters - Setters
	 */

    public Context getContext() {
        return context;
    }


    public void setContext(Context context) {
        this.context = context;
    }


    public ConfirmQuestionDialogButtonInterface getCallback() {
        return callback;
    }


    public void setCallback(ConfirmQuestionDialogButtonInterface callback) {
        this.callback = callback;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }

    public String getPositiveButtonMessage() {
        return positiveButtonMessage;
    }


    public void setPositiveButtonMessage(String positiveButtonMessage) {
        this.positiveButtonMessage = positiveButtonMessage;
    }


    public String getNegativeButtonMessage() {
        return negativeButtonMessage;
    }


    public void setNegativeButtonMessage(String negativeButtonMessage) {
        this.negativeButtonMessage = negativeButtonMessage;
    }

}
