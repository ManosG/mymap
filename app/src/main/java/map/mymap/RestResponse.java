package map.mymap;


public class RestResponse {
    int status;
    String message;



    public RestResponse(int status, String message) {
        super();
        this.status = status;
        this.message = message;
    }



    public synchronized int getStatus() {
        return status;
    }
    public synchronized void setStatus(int status) {
        this.status = status;
    }
    public synchronized String getMessage() {
        return message;
    }
    public synchronized void setMessage(String message) {
        this.message = message;
    }
}
