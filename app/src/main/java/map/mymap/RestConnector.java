package map.mymap;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


public class RestConnector {
    public static RestResponse makeHttpRequest(String url, String method, String body, HashMap<String, String> headers) throws IOException {
        BufferedReader reader = null;
        URL path = new URL(url);
        HttpURLConnection urlConnection = (HttpURLConnection) path.openConnection();

        try {
            urlConnection.setRequestMethod(method);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

        	/*
        	 * Set headers
        	 */
            //default header
            urlConnection.setRequestProperty("Content-Type","text/plain;charset=utf-8");
            //user defined headers
            if(headers != null){
                for(Map.Entry<String, String> header : headers.entrySet()){
                    urlConnection.setRequestProperty(header.getKey(), header.getValue());
                }
            }

        	/*
        	 * Connect
        	 */
            urlConnection.connect();

            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            out.write(body.getBytes("UTF-8"));
            out.flush();
            out.close();

            /*
             * Get response code
             */
            int status = urlConnection.getResponseCode();

            /*
             * Try to get input
             */
            String output = null;
            try{
                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                output = parseResponse(reader);
                urlConnection.disconnect();
            }
            catch(Exception e){}

            /*
             * Return response object
             */
            return new RestResponse(status, output);
        } catch (Exception e1) {
            e1.printStackTrace();
            System.err.println("Could not connect to server");
        }
        return null;
    }


    public static String parseResponse(BufferedReader reader){
        String reply = null;
        try {
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
            reader.close();
            reply = sb.toString();
        } catch (Exception e) {
            System.err.println("Could not parse server response");
        }
        return reply;
    }
}
