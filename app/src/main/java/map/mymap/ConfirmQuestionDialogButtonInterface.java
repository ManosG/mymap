package map.mymap;


public interface ConfirmQuestionDialogButtonInterface {
    void positiveButton(String answer, String id);
    void negativeButton();
}
